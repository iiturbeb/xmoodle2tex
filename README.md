# XMoodLe2TeX

Converting Moodle's XML question banks to LaTeX.

## License

To be determined.

## Authors

2019-2020: Matthieu Guerquin-Kern (guerquin-kern AT crans.org)
2023: Ion Iturbe Beristain (profesor at Mondragon University)

## Contents

This work consists of the Python script [XMoodLe2TeX.py](XMoodLe2TeX.py).

## Origin

The development happens on a server hosting a [GITLAB](https://gitlab.com)
instance.
The project is located at <https://framagit.org/mattgk/xmoodle2tex>.
There, among other things, you can see the activity of the project and you
can download the latest version of the project files.

You can use GIT to access the project files using the command:

    $ git clone "https://framagit.org/mattgk/xmoodle2tex.git"

To gain write access to the project and contribute, you must have an account on
this server. Visit <https://framagit.org> and register.

## Requirements

The building setup was tested with an installation of Python 3.10.4 under
a GNU/LINUX environment (Ubuntu, specifically).
Although, the building process might be also set on Windows and macOs
environments, the authors will only support the use of GNU/Linux. Windows/macOs
users can install GNU/Linux with a minimal impact on their main working
environment using either
1. a LiveUSB installation (see 
<https://en.wikipedia.org/wiki/List_of_tools_to_create_Live_USB_systems>), or
2. a virtual machine (see virtualbox for instance).


The following Python libraries are used:
- [`argparse'](https://docs.python.org/fr/3/howto/argparse.html)
- [`xml.etree.ElementTree`](https://docs.python.org/3/library/xml.etree.elementtree.html)
- [`re`](https://docs.python.org/3/library/re.html)
- `BeautifulSoup`
- base64

LaTeX compilation was tested with the last developement version of the package
[`moodle`](https://framagit.org/mattgk/moodle).
