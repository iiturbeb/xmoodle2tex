#!/usr/bin/env python3
#
###############
# XMoodLe2TeX #
###############
#
# Converts Moodle quiz XML files into a TeX file using the 'moodle' package.
# The TeX file allows convenient edition of the quiz. LaTeX compilation generates
# a PDF for proofreading and an XML file that can be imported back to Moodle.
#
# Moodle's XML format is described here: https://docs.moodle.org/35/en/Moodle_XML_format
#
# For clarity and maintainability, this code uses the OOP concepts of inheritance and polymorphism.
#
# QuizElement (type, name)
#	|-> QuizCategory (category)
#       |-> QuizQuestion (questiontext, penalty, defaultgrade, hidden, generalfeedback, 
#		|-> QuizMultianswer -> list --> Answer (fraction, text, feedback, tolerance) or Subquestion (text, answer)
#		|	|-> QuizMultichoice (single, shuffleanswers, answernumbering)
#		|	|-> QuizShortanswer (usecase)
#		|	|-> QuizNumerical (units handling is not supported yet by LaTeX package...)
#               |       |-> QuizTruefalse (two answers, only one correct, no answer text)
#               |	|-> QuizMatching (shuffleanswers)
#               |-> QuizEssay (responserequired, responseformat, responsefieldlines, attachments, attachmentsrequired, graderinfo,
#		|		responsetemplate) TODO
#               |-> QuizCloze (questiontext is to be parsed to generate embedded QuizMultianswer elements with no name) TODO
#               |-> QuizDescription (not supported yet by the LaTeX package...) !WONT ADD SOON!
#
# Matthieu Guerquin-Kern, 2019-2020

import argparse # see https://docs.python.org/fr/3/howto/argparse.html
import xml.etree.ElementTree as ET # see https://docs.python.org/3/library/xml.etree.elementtree.html
import re # see https://docs.python.org/3/library/re.html
from bs4 import BeautifulSoup
import base64 # see https://docs.python.org/3/library/base64.html
import sympy
import sys
import os
import textwrap

currentCategory=None

class CodeGenerationOptions():
	def __init__(self):
		self.divideCodeInQuestions = True
		self.saveQuestionDescriptionCodeInSeparateFile = True
		self.saveImagesInFolder = True
		self.convertCalculatedSimpleToShortAnswer = False
		self.encodingString = "UTF-8" # could be "UTF-8" or "iso-8859-1"

codegenOptions = CodeGenerationOptions()

# Maybe this could be modified using examples of https://github.com/pankaj28843/html2latex/blob/master/src/html2latex/html2latex.py
def html2TeX(s,files=None):
	""" Converting HTML strings into proper LaTeX equivalent """
	if s==None:
		return None,[]
#\html@def\emph#1{<EM>#1</EM>}%
#\html@def\textit#1{<I>#1</I>}%
#\html@def\textbf#1{<B>#1</B>}%
#\html@def\texttt#1{<CODE>#1</CODE>}%
#\html@def\blank{____________}%
#\html@def\S{\&sect;}%
#\html@def\euro{\&euro;}%
#\html@def\texteuro{\&euro;}%
#\html@def\o{\&oslash;}%
#\html@def\O{\&Oslash;}%
#\html@def\ss{\&szlig;}%
#\html@def\l{\&lstrok;}%
#\html@def\L{\&Lstrok;}%
	s = s.replace('&nbsp;',' ')
	soup = BeautifulSoup(s,"html.parser")
	listOfImageFiles = []
	for im in soup.find_all('img'):
		match = re.findall(r'/(.*)$', im['src'])
		imname = match[0].replace("%20"," ").replace("%28","(").replace("%29",")") # TODO: do it more general, related to filename encodings in moodle-xml
		if(codegenOptions.saveImagesInFolder):
			imnamePathForIncludeGraphics = "./images/" + imname
		else:
			imnamePathForIncludeGraphics = imname
		im.replace_with('\n\\begin{center}\n\includegraphics[width=\linewidth]{' + imnamePathForIncludeGraphics + '}\n\end{center}\n')
		for f in files:
			if f.get('name') == imname:
				listOfImageFiles.append(f)
	for a in soup.find_all('a'):
    		a.replace_with('\href{' + a.get("href")+ '}{' + a.string + '}')
#	for ol in soup.find_all('ol'):
#		if ol.text != None:
#   			ol.replace_with('\\begin{enumerate}\n' + ol.text + '\end{enumerate}')
#		else:
#			ol.decompose()
	for li in soup.find_all('li'):
		if li.text != None:
    			li.replace_with('\item ' + li.text)
		else:
			li.decompose()
	for i in soup.find_all('i'):
		if i.string!= None:
			i.replace_with('\emph{' + i.string + '}')
	s = str(soup)
	# replace the "^" symbols that are not inside equation markers $$
	marked_s = re.sub(r'\$.*?\$', lambda x: x.group().replace('^', 'MARKERFORTEMPORALUSE'), s)
	marked_replaced_s = marked_s.replace('^',"\\textsuperscript ")
	s = marked_replaced_s.replace('MARKERFORTEMPORALUSE','^')

	s = s.replace('<ul>','\\begin{itemize}').replace('</ul>','\end{itemize}').replace('<ol>','\\begin{enumerate}').replace('</ol>','\end{enumerate}')
	s = s.replace('<em>','\\begin{em}').replace('</em>','\end{em}')
	s = s.replace('<span>', '').replace('</span>', '')
	s = s.replace('<div>', '').replace('</div>', '\n')
	s = s.replace('</p><p>',' ').replace('</p>', '')
	s = re.sub('<p[^>]+>', '',s).replace('<p>', '')
	s = s.replace('\(','$').replace('\)','$').replace('$$','$')
	s = s.replace('</P><P>','\\\\').replace('<P>', '').replace('</P>', '').replace('<br/>','').replace('<BR/>','')
	s = s.replace('&rsquo;','\'').replace('&gt;','>').replace('&lt;','<').replace('&nbsp;',' ')
	s = s.replace('&ldquo;','"').replace('&lsquo;',"`").replace('&rdquo;','"').replace('&rsquo;',"'")
	s = s.replace('&ccedil;','\{c}c').replace('&Ccedil;','\{c}C').replace('&laquo;','\og').replace('&raquo;','\\fg{}')
	s = s.replace('\&AElig;','\AE').replace('\&aelig;','\\ae').replace('\&OElig;','\OE').replace('\&oelig;','\\oe')
	s = s.replace('\&Aring;','\AA').replace('\&aring;','\\aa')
	s = s.replace('&amp;','&').replace('&','\&').replace('%','\%').replace('#','\#')
	s = s.replace('º','\textdegree')
	# Note: changing & for\& breaks LaTeX math arrays
	# TODO change this ugly code for a dictionary-based substitution:
	#             https://www.oreilly.com/library/view/python-cookbook/0596001673/ch03s15.html
	
	# DEBUG	
	#s = re.sub(r'(?s)<\b.*?>', '', s) # filter remaining html tags
	# TODO put here base64 file decoder ?
	return s,listOfImageFiles

#########################
# DEFINITION OF CLASSES #
#########################
class QuizElement: # Parent of all other classes
	""" Quiz Elements are category descriptions or questions.
	This is a root class doing generic stuff. Child classes are doing more specific tasks. """
	def __init__(self, ETO, name=None, type=None): # either construct with ETO or with keyword arguments
		self.name = name # unless specified with keywords, QuizElement by itself always sets 'name' to 'None'
		self.type = type
		if ETO!=None:
			if ETO.tag != 'question':
				malformedXML('quiz element is ' + ETO.tag)
			self.type = ETO.get('type').lower() # retrieve element type ('category' or question types)
			if self.type==None:
				malformedXML('quiz element with no ''type'' specified ')

	def printHeader(self):
		if self.type==None:
			malformedXML('Undefined Type')
		elif self.type=='category':
			#if currentCategory != None:
#			print('\end{quiz}')
			if self.name != 'top':
				print('\\begin{quiz}{' + self.name + '}')

	def printFooter(self):
		if self.type!='category':
			print('\end{' + self.type + '}\n')

class QuizCategory(QuizElement): # child class of QuizElement, sets a category for a group of questions
	""" Quiz Categories describe categories for Moodle. This class adequately imports a Quiz Element that is a category. """
	def __init__(self, ETO, name=None): # either construct with ETO or with keyword arguments
		super().__init__(ETO, name, type='category') # inherits the constructor and adds specific bits
		if ETO!=None:
			childs = ETO.findall('category')
			if len(childs)>1:
				malformedXML('expected only one category tag at a time ')		
			grandchild = childs[0].find('text')
			match = re.findall(r'/([^/]*)', grandchild.text) # retrieve category name
			self.name, self.listOfImageFiles = html2TeX(match[-1])
			currentCategory = self.name # change corresponding global variable

	def print(self):
		self.printHeader()
		self.printFooter()

def transformVariableNameInDescription(match):
    # Perform your transformation on `match.group()`
    # and return the transformed string
	return "$" + sympy.latex(sympy.sympify(match.group(1))) + "$"

def transformVariableNameInAnswer(match):
    # Perform your transformation on `match.group()`
    # and return the transformed string
	return match.group(1)

class QuizQuestion(QuizElement): # child class of QuizElement, parent of all Questions
	""" Quiz Question is a generic class describing Moodle questions. This class deals with generic aspects. Child classes are doing specific tasks. """
	def __init__(self, ETO, name=None, type=None, questiontext=None, generalfeedback=None, penalty=None, defaultgrade=None, hidden=None):
	# either construct with ETO or with keyword arguments (the latter is useful for embedded questions in Cloze)
		super().__init__(ETO, name, type) # inherits the constructor and adds specific bits
		# Setting tags common to all question types
		self.listOfImageFiles = []
		for field in ['questiontext','generalfeedback','penalty','defaultgrade','hidden']:
			setattr(self, field, eval(field))
		if ETO!=None:
			for tag in ['name','questiontext']: # these tags are mandatory
				childs = ETO.findall(tag)
				if len(childs)==0:
					malformedXML('expected one ' + tag + ' tag for the question of type ' + self.type)
				if len(childs)>1:
					malformedXML('expected only one ' + tag + ' tag at a time ')
				grandchild = childs[0].find('text')
				if(self.type == "calculatedsimple" and tag == "questiontext"):
					replacedText = re.sub("\{([^\}]+)\}",transformVariableNameInDescription,grandchild.text)
				else:
					replacedText = grandchild.text
				s, self.listOfImageFiles = html2TeX(replacedText,files=childs[0].findall('file'))
				setattr(self, tag, s) # setting self.name or self.questiontext
			for tag in ['penalty','defaultgrade','hidden']: # optional tags with numerical value
				childs = ETO.findall(tag)
				if len(childs)>1:
					malformedXML('expected only one ' + tag + ' tag at a time ')
				if len(childs)==1:
					setattr(self, tag, str("{0:.5g}".format(float(childs[0].text)))) # setting self.penalty, self.defaultgrade or self.hidden
			# general feedback
			self.generalfeedback = None # default generalfeedback setting
			childs = ETO.findall('generalfeedback')
			if len(childs)>1:
				malformedXML('expected only one generalfeedback tag at a time ')
			if len(childs)==1:
				grandchild = childs[0].find('text')
				self.generalfeedback,listOfImages = html2TeX(grandchild.text,files=childs[0].findall('file'))
			# TODO : tags (non-hierarchical keywords)

	def printHeader(self):
		if(codegenOptions.convertCalculatedSimpleToShortAnswer and (self.type == "calculatedsimple")):
			print('\\begin{shortanswer}[')
		else:
			print('\\begin{' + self.type + '}[')
		if self.defaultgrade!=None:
			print('\tpoints=' + self.defaultgrade + ',')
		if self.generalfeedback!=None:
			print('\tfeedback={' + self.generalfeedback + '},')
		if self.penalty!=None:
			print('\tpenalty=' + self.penalty + ',')

	def printEndOfHeaderAfterSpecificOptions(self,titleFileName=None):
		if(titleFileName is None):
			print(']{' + self.name + '}\n')
		else:
			print(']{\\subimport{./}{' + titleFileName + '}}\n')
			

	def printQuestionBody(self):
		print(self.questiontext)
		self.saveImageFiles()

	def print(self,questionMainFileName=None,questionDescriptionFileName=None,questionTitleFileName=None):
		previousOutput = sys.stdout
		if(questionMainFileName is None):
			self.printHeader()
			self.printQuestionBody()
			self.printFooter()
		else:
			with open(questionMainFileName,'w') as questionMainFile:
				sys.stdout = questionMainFile
				if(questionDescriptionFileName is None):
					self.printHeader()
					self.printQuestionBody()
				else:
					if(questionTitleFileName is None):
						self.printHeader()
					else:
						self.printHeader(questionTitleFileName)
					print('\subimport{./}{' + questionDescriptionFileName + '}')
					with open(questionDescriptionFileName,'w') as questionDesqriptionFile:
						sys.stdout = questionDesqriptionFile
						self.printQuestionBody()
						sys.stdout = questionMainFile
					if(not (questionTitleFileName is None)):
						with open(questionTitleFileName,'w') as questionTitleFile:
							sys.stdout = questionTitleFile
							print(self.name)
							sys.stdout = questionMainFile
				self.printFooter()
				sys.stdout = previousOutput


	def saveImageFiles(self):
		for f in self.listOfImageFiles:
			if(codegenOptions.saveImagesInFolder):
				if(not os.path.exists('./images')):
					os.makedirs('./images')
				imageFilePath = './images/' + f.get('name')
			else:
				imageFilePath = f.get('name')
			fh = open(imageFilePath, "wb")
			fh.write(base64.b64decode(f.text))
			fh.close()

		#print('\t\t]{' + self.name + '}')
	
	def printFooter(self):
		if(codegenOptions.convertCalculatedSimpleToShortAnswer and (self.type == "calculatedsimple")):
			print('\end{shortanswer}\n')
		else:
			print('\end{' + self.type + '}\n')


class Answer(): # class to store answer elements made of fraction, text, feedback, and tolerance
	""" Answer is a class describing generic question answers.
	Its elements are: elements made of fraction, text, feedback, and tolerance. Depending on the answer, some elements might be unused. """
	def __init__(self, ETO, fraction=100, text=None, feedback=None, tolerance=None):
		for field in ['fraction','text','feedback','tolerance']:
			setattr(self, field, eval(field))
		if ETO!=None:
			childs = ETO.findall('text')
			if len(childs)==0: # text tags is mandatory
				malformedXML('expected one text tag for the answer')
			if len(childs)>1:
				malformedXML('expected only one text tag at a time ')
			self.text,listOfImages = html2TeX(childs[0].text,files=ETO.findall('file')) # setting self.name or self.questiontext
			childs = ETO.findall('feedback') # for other types answers
			self.fraction = ETO.get('fraction')
			if len(childs)>1:
				malformedXML('expected only one feedback/answer tag at a time ')
			if len(childs)==1:
				grandchild = childs[0].find('text')
				self.feedback,listOfImages = html2TeX(grandchild.text,files=ETO.findall('file'))	# the field 'feedback' stores the answer for Matching types (no feedback in this case)
								# or stores, more naturally, the answer's specific feedback for Multichoice, Shortanswer and Numerical
			childs = ETO.findall('tolerance') # for Numerical type
			if len(childs)>1:
				malformedXML('expected only one ' + tag + ' tag at a time ')
			if len(childs)==1:
				self.tolerance = childs[0].text

class MAnswer(Answer): # for multichoice, shortanswer and truefalse
	def print(self):
		tmp = '\item['
		if self.feedback != None:
			tmp += 'feedback={' + self.feedback + '},'
		if (self.fraction != '0') and (self.fraction != '100'):
			tmp += 'fraction=' + self.fraction
		if len(tmp)==6:
			tmp = tmp[:-1]
		else:
			tmp += ']'
		if self.fraction == '100':
			tmp += '*'
		if self.text != None:
			tmp += ' ' + self.text
		print(tmp)

class NAnswer(Answer): # for numerical that does not specify correct answers with *
	def print(self):
		tmp = '\item['
		for tag in ['feedback','tolerance']:
			value = getattr(self, tag)
			if value != None:
				tmp += tag + '={' + value + '},'
		tmp += 'fraction=' + self.fraction + '] '
		if self.text != None:
			tmp += self.text
		print(tmp)

class Subquestion(): # class to store matching subquestion elements made of text and answer
	def __init__(self, ETO, text=None, answer=None):
		for field in ['text','answer']:
			setattr(self, field, eval(field))
		if ETO!=None:
			childs = ETO.findall('text')
			if len(childs)==0: # text tags is mandatory
				malformedXML('expected one text tag for the answer')
			if len(childs)>1:
				malformedXML('expected only one text tag at a time ')
			self.text,listOfImages = html2TeX(childs[0].text,files=ETO.findall('file')) # setting self.name or self.questiontext
			childs = ETO.findall('answer')
			if len(childs)>1:
				malformedXML('expected only one feedback tag at a time ')
			if len(childs)==1:
				grandchild = childs[0].find('text')
				self.answer = html2TeX(grandchild.text,files=childs[0].findall('file'))

	def print(self):
		if self.text == None:
			print('\item \\answer ' + str(self.answer))
		else:
			print('\item ' + str(self.text) + ' \\answer ' + str(self.answer))

class CalculatedSimpleAnswer(): # class to store matching subquestion elements made of text and answer
	def __init__(self, ETO, text=None, answer=None):
		for field in ['text','answer']:
			setattr(self, field, eval(field))
		if ETO!=None:
			childs = ETO.findall('text')
			if len(childs)==0: # text tags is mandatory
				malformedXML('expected one text tag for the answer')
			if len(childs)>1:
				malformedXML('expected only one text tag at a time ')
			replacedText = re.sub("\{([^\}]+)\}",transformVariableNameInAnswer,childs[0].text)
			self.text = sympy.latex(sympy.sympify(replacedText))
			childs = ETO.findall('answer')
			if len(childs)>1:
				malformedXML('expected only one feedback tag at a time ')
			if len(childs)==1:
				grandchild = childs[0].find('text')
				self.answer,listOfImages = html2TeX(grandchild.text,files=childs[0].findall('file'))

	def print(self):
		if self.text == None:
			print('\\item $ $ ' + str(self.answer))
		else:
			print('\\item $' + str(self.text) + '$')

class QuizMultianswer(QuizQuestion): # child class of QuizQuestion, parent of Multichoice, ShortAnswer, and Numerical
	def __init__(self, ETO, name=None, type=None, questiontext=None, generalfeedback=None, penalty=None, defaultgrade=None, hidden=None, list=None):
		# inherits the constructor and adds specific bits
		super().__init__(ETO, name, type, questiontext, generalfeedback, penalty, defaultgrade, hidden)
		# Setting tags specific to multianswer questions (numerical, shortanswer, multichoice)
		if list == None:
			self.list = []
		else:
			self.list = list
		if ETO!=None:
			if (self.type == 'matching')|(self.type == 'ddmatch'):
				for eto in ETO.iter('subquestion'):
					self.list.append(Subquestion(eto))
			elif self.type == 'numerical':
				for eto in ETO.iter('answer'):
					self.list.append(NAnswer(eto))
			elif self.type == 'calculatedsimple':
				for eto in ETO.iter('answer'):
					self.list.append(CalculatedSimpleAnswer(eto))
			else: # multichoice, shortanswer, and truefalse
				for eto in ETO.iter('answer'):
					self.list.append(MAnswer(eto))

	def printFooter(self):
		for a in self.list:
			a.print()
		super().printFooter() # inherits the parent's method

class QuizMultichoice(QuizMultianswer): # child class of QuizMultiAnswer
	def __init__(self, ETO, name=None, type=None, questiontext=None, generalfeedback=None, penalty=None, defaultgrade=None, hidden=None, list=None, single=None, shuffleanswers=None, answernumbering=None):
		# inherits the constructor and adds specific bits
		super().__init__(ETO, name, type, questiontext, generalfeedback, penalty, defaultgrade, hidden, list)
		# Setting tags specific to multichoice questions
		self.type = 'multi'
		for field in ['single','shuffleanswers','answernumbering']:
			setattr(self, field, eval(field))
		if ETO!=None:
			for tag in ['single','shuffleanswers','answernumbering']:
				childs = ETO.findall(tag)
				if len(childs)>1:
					malformedXML('expected only one ' + tag + ' tag at a time ')
				if len(childs)==1:
					setattr(self, tag, childs[0].text) # setting self.single, self.shuffleanswers or self.answernumbering

	def printHeader(self,titleFileName=None):
		super().printHeader() # inherits the grandparent's method
		if self.single == 'false':
			print('\tmultiple,')
		if self.shuffleanswers == '0':
			print('\tshuffle=false,')
		if self.answernumbering != None:
			if self.answernumbering != 'abc': # default
				print('\tnumbering=' + self.answernumbering + ',')
		super().printEndOfHeaderAfterSpecificOptions(titleFileName)

class QuizShortanswer(QuizMultianswer): # child class of QuizMultianswer
	def __init__(self, ETO, name=None, type=None, questiontext=None, generalfeedback=None, penalty=None, defaultgrade=None, hidden=None, list=None, usecase=None):
		# inherits the constructor and adds specific bits
		super().__init__(ETO, name, type, questiontext, generalfeedback, penalty, defaultgrade, hidden, list)
		# Setting tag specific to shortanswer questions
		self.usecase = usecase
		if ETO!=None:
			childs = ETO.findall('usecase')
			if len(childs)>1:
				malformedXML('expected only one usecase tag at a time ')
			if len(childs)==1:
				self.usecase = childs[0].text

	def printHeader(self,titleFileName=None):
		super().printHeader() # inherits the grandparent's method
		if self.usecase == '1':
			print('\tusecase,')
		super().printEndOfHeaderAfterSpecificOptions(titleFileName) 

class QuizNumerical(QuizMultianswer): # child class of QuizMultiAnswer
	def printHeader(self,titleFileName=None):
		super().printHeader() # inherits the grandparent's method
		super().printEndOfHeaderAfterSpecificOptions(titleFileName) 

class QuizCalculatedSimple(QuizMultianswer): # child class of QuizMultiAnswer
	def printHeader(self,titleFileName=None):
		super().printHeader() # inherits the grandparent's method
		super().printEndOfHeaderAfterSpecificOptions(titleFileName) 

class QuizTruefalse(QuizMultianswer): # child class of QuizMultianswer
	def __init__(self, ETO, name=None, type=None, questiontext=None, generalfeedback=None, penalty=None, defaultgrade=None, hidden=None, list=None):
		# inherits the constructor and adds specific bits
		super().__init__(ETO, name, type, questiontext, generalfeedback, penalty, defaultgrade, hidden, list)
		# Setting tag specific to truefalse questions
		self.penalty = '1'
		self.list = self.list[:2]
		if self.list[1].text == 'true':
			self.list = [self.list[1],self.list[0]]
		if (self.list[1].text != 'false') or (self.list[0].text != 'true'):
			malformedXML('expected only "true" and "false" answers')
		for i in [0,1]:
			self.list[i].text = ' % ' + self.list[i].text
			if self.list[i].feedback != None:
				self.list[i].text = self.list[i].feedback + self.list[i].text
				self.list[i].feedback = None
	def printHeader(self,titleFileName=None):
		super().printHeader() # inherits the grandparent's method
		super().printEndOfHeaderAfterSpecificOptions(titleFileName) 

class QuizMatching(QuizMultianswer): # child class of QuizMultianswer
	def __init__(self, ETO, name=None, type=None, questiontext=None, generalfeedback=None, penalty=None, defaultgrade=None, hidden=None, list=None, shuffleanswers=None, dd=None):
		# inherits the constructor and adds specific bits
		super().__init__(ETO, name, type, questiontext, generalfeedback, penalty, defaultgrade, hidden, list)
		# Setting tag specific to matching questions
		self.shuffleanswers = shuffleanswers
		self.dd = dd
		if ETO!=None:
			if self.type == 'ddmatch':
				self.dd = 1
				self.type = 'matching'
			childs = ETO.findall('shuffleanswers')
			if len(childs)>1:
				malformedXML('expected only one shuffleanswers tag at a time ')
			if len(childs)==1:
				self.shuffleanswers = childs[0].text # setting self.single, self.shuffleanswers or self.answernumbering

	def printHeader(self,titleFileName=None):
		super().printHeader() # inherits the grandparent's method
		if self.shuffleanswers == '0':
			print('\tshuffle=false,')
		if self.dd != None:
			print('\tdd,')
		super().printEndOfHeaderAfterSpecificOptions(titleFileName)

###########################
# DEFINITION OF FUNCTIONS #
###########################

# This function makes use of polymorphism to define an object of class
# QuizCategory or QuizQuestion, depending on the element tree object
def makeQuizElement(eto):
	type = eto.get('type').lower()
	if type==None:
		malformedXML('question with no ''type'' specified ')
	if type == 'category':
		return QuizCategory(eto)	
	if type == 'multichoice':
		return QuizMultichoice(eto)
	if type == 'shortanswer':
		return QuizShortanswer(eto)
	if type == 'numerical':
		return QuizNumerical(eto)
	if (type == 'matching')|(type == 'ddmatch'):
		return QuizMatching(eto)	
	if type == 'truefalse':
		return QuizTruefalse(eto)
	if type == 'calculatedsimple':
		return QuizCalculatedSimple(eto)
#	if type == 'description':
		#print('the description type is not yet supported')
		#quit()	
	#if type == 'essay':
	else: # cloze or other unknown type?
		return None #QuizQuestion(eto)
	
################
# PARSEXMLQUIZ #
################
def parseXMLquiz(filename):
	#print(filename)
	tree = ET.parse(filename)
	root = tree.getroot()
	if root.tag.lower() != 'quiz':
		malformedXML('root of tree is ' + root.tag)
	#print(root.tag)
	#for child in root:
	#	print(child.tag, child.attrib)
	quizElements = []
	for eto in root.iter('question'):
		qe = makeQuizElement(eto)
		quizElements.append(qe)
	return quizElements

def malformedXML(message):
	print('The XML file does not seem to correspond to a Moodle quiz')
	print('	-> ' + message)
	quit()

def createResultFiles(quizElements,args):
	if(not (args.outputFolder == None)):
		initialWorkingDirectory = os.getcwd()
		outputFolderAbsolutePath = os.path.abspath(args.outputFolder)
		if(not os.path.exists(outputFolderAbsolutePath)):
			os.makedirs(outputFolderAbsolutePath)
		os.chdir(outputFolderAbsolutePath)
	headerstring = textwrap.dedent("""\
				% !TeX encoding = {codegenOptions.encodingString}
				% !TEX TS-program = xelatex
				% To compile use the '--enable-write18' option
				\\documentclass{{article\}}
				\\usepackage[nostamp]{{moodle}}
				\\ifPDFTeX % FOR LATEX and PDFLATEX
					\\usepackage[utf8]{{inputenc}} % necessary
					\\usepackage[T1]{{fontenc}} % necessary
				\\else % assuming XELATEX or LUALATEX
					% \\usepackage{{fontspec}}
				\\fi
				\\usepackage{{textcomp}}
				\\usepackage{{graphicx}}
				\\usepackage{{import}}
				\\usepackage{{moodle}}
				\\usepackage{{hyperref,babel}}
				\\usepackage[cm]{{fullpage}}
				\\begin{{document}}""")
	if(not (args.output == None)):
		original_stdout = sys.stdout
		with open(args.output,'w') as f:
			sys.stdout = f
			print(headerstring)
			quenstionFolderNumber = 0
			for qe in quizElements:
				if qe != None:
					if((qe.type == 'category') or (not codegenOptions.divideCodeInQuestions)):
						qe.print()
					else:
						quenstionFolderNumber = quenstionFolderNumber + 1
						quenstionFolderName = './q' + str(quenstionFolderNumber)
						print('\subimport{'+quenstionFolderName + '/}{question.tex}')
						os.makedirs(quenstionFolderName)
						os.chdir(quenstionFolderName)
						if(codegenOptions.saveQuestionDescriptionCodeInSeparateFile):
							qe.print('question.tex','description.tex','title.tex')
						else:
							qe.print('question.tex')
						os.chdir("..")
			print('\end{quiz}')
			print('\end{document}')
			sys.stdout = original_stdout
	else:
		print(headerstring)
		for qe in quizElements:
			if qe != None:
				qe.printHeader()
				qe.printQuestionBody()
				qe.printFooter()
		print('\end{quiz}')
		print('\end{document}')

	if(not (args.outputFolder == None)):
		os.chdir(initialWorkingDirectory)


########
# MAIN #
########
def main():
	# Argument Parsing #
	parser = argparse.ArgumentParser(description='XMoodLe2TeX: Convert Moodle Quizzes from XML to LaTeX.')
	parser.add_argument('filepath', nargs='+')
	# parser.add_argument('-v', '--verbose', help='print all info', action='store_true')
	parser.add_argument('-o', '--output', help='specify an output file')
	parser.add_argument('-d', '--outputFolder', help='specify a folder to save all files')
	parser.add_argument('--divideCodeInQuestions',default=False, action=argparse.BooleanOptionalAction, help='(default True) The Latex code of each question is created inside a separted folder')
	parser.add_argument('--saveQuestionDescriptionCodeInSeparateFile',default=False, action=argparse.BooleanOptionalAction, help='(default True) the "description" part of each question is saved in a separated tex file')
	parser.add_argument('--saveImagesInFolder',default=True, action=argparse.BooleanOptionalAction, help='(default True) image files are saved inside a folder named "images"')
	parser.add_argument('--convertCalculatedSimpleToShortAnswer',default=False, action=argparse.BooleanOptionalAction, help='(default True) The "calculatedsimple" question type are saved as "shortanswer" because if not, the generated latex code does not compile using moodle package')
	parser.add_argument('-e', '--encoding', default="UTF-8", help='encoding string to write in the file')
	args = parser.parse_args()

	global codegenOptions
	codegenOptions.divideCodeInQuestions = args.divideCodeInQuestions
	codegenOptions.saveQuestionDescriptionCodeInSeparateFile = args.saveQuestionDescriptionCodeInSeparateFile
	codegenOptions.saveImagesInFolder = args.saveImagesInFolder
	codegenOptions.convertCalculatedSimpleToShortAnswer = args.convertCalculatedSimpleToShortAnswer
	codegenOptions.encodingString = args.encoding

	currentCategory = None
	fileToBeProcessed = os.path.abspath(args.filepath[0])
	quizElements = parseXMLquiz(filename=fileToBeProcessed)
	createResultFiles(quizElements,args)

if __name__ == "__main__":
    main()
